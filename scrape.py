#!/usr/bin/env python3

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import urllib
import re
import sys
import os
import datetime
from dataclasses import dataclass


ignored_urls = [
    "https://de.wikipedia.org/wiki/Tom_Cruise"
    ]

@dataclass
class Context:
    logfile: str
    urls: str

def search_google(query, page=1):
    '''
    Returns: list of urls
    '''
    params = {'q': query}
    if page > 1:
        params['start'] = (page - 1) * 10
    baseurl = 'https://www.google.de/search'
    url = baseurl + '?' + urllib.parse.urlencode(params)
    root = get_dom(url)
    result = []
    for el in root.select('#search h3'):
        if 'href' in el.parent.attrs:
            result.append(el.parent.attrs['href'])
    return result

def search_duckduckgo(query):
    params = {'q': query}
    baseurl = 'https://duckduckgo.com/'
    url = baseurl + '?' + urllib.parse.urlencode(params)
    root = get_dom(url)
    result = []
    for el in root.select('.results--main h2 a'):
        if 'href' in el.attrs:
            result.append(el.attrs['href'])
    return result

def search(engine, query):
    fn = globals()[f'search_{engine}']
    return fn(query)

def get_dom(url):
    '''
    Starts browser, loads dynamic page, then return page content as html.
    '''
    with webdriver.Firefox() as driver:
        driver.get(url)
        html = driver.find_element(By.TAG_NAME, 'html').get_attribute('outerHTML')
        root = BeautifulSoup(html, features='lxml')
        return root

def search_keywords(s, keywords):
    '''
    args:
      s(string)
      keywords(list of strings)
    returns first match, otherwise None
    '''
    r = re.compile('|'.join([re.escape(kw) for kw in keywords]), re.IGNORECASE)
    m = re.search(r, s)
    if m is not None:
        (i, j) = m.span()
        return s[i:j]

def page_matches(p, keywords_list):
    '''
    example:
     Returns matched if "My Name" AND any of the keywords 'foo', 'bar' match.

    page_matches(p, [['My Name'], ['foo', 'bar']])
    '''
    s = p.text
    matched_strings = []
    for keywords in keywords_list:
        m = search_keywords(s, keywords)
        if m is None:
            return None
        else:
            matched_strings.append(m)
    return ', '.join(matched_strings)

def create_context():
    output = os.path.join(os.path.dirname(__file__), 'output')
    ts = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    outdir = os.path.join(output, ts)
    os.makedirs(outdir)
    logfile = open(os.path.join(outdir, 'log.txt'), 'w')
    urls = open(os.path.join(outdir, 'urls-found.txt'), 'w')
    return Context(logfile, urls)

def log(ctx, msg):
    msg += '\n'
    print(msg, end='')
    ctx.logfile.write(msg)
    ctx.logfile.flush()

def url_is_ignored(url):
    for u in ignored_urls:
        if u in url:
            return True
    return False

def main():
    ctx = create_context()
    log(ctx, f'Logging to {ctx.logfile.name}')
    log(ctx, f'Writing result urls to {ctx.urls.name}')

    query = 'Tom Cruise'
    keywords_list = [['Tom Cruise'], ['top gun', 'scientology']]

    for engine in ['google', 'duckduckgo']:
      log(ctx, f'Searching in {engine}')
      for url in search(engine, query):
          if url_is_ignored(url):
              log(ctx, f'Ignored {url}')
              continue
          log(ctx, f'Checking {url}')
          p = get_dom(url)
          m = page_matches(p, keywords_list)
          if m is not None:
              log(ctx, f'Page matches with keywords: {m}')
              ctx.urls.write(f'{m}: {url}\n')
              ctx.urls.flush()
          else:
              log(ctx, f'No match')


    ctx.logfile.close()
    ctx.urls.close()

if __name__ == '__main__':
    main()
