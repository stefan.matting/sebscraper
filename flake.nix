{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };



  outputs = { self, nixpkgs }:
    let pkgs = import nixpkgs { system = "x86_64-linux"; };

        python' = let
          packageOverrides = prev: final: {
            selenium = final.selenium.overridePythonAttrs (old: {
              src = pkgs.fetchFromGitHub {
                owner = "SeleniumHQ";
                repo = "selenium";
                rev = "refs/tags/selenium-4.8.0";
                hash = "sha256-YTi6SNtTWuEPlQ3PTeis9osvtnWmZ7SRQbne9fefdco=";
              };
              postInstall = ''
                install -Dm 755 ../rb/lib/selenium/webdriver/atoms/getAttribute.js $out/${pkgs.python3Packages.python.sitePackages}/selenium/webdriver/remote/getAttribute.js
                install -Dm 755 ../rb/lib/selenium/webdriver/atoms/isDisplayed.js $out/${pkgs.python3Packages.python.sitePackages}/selenium/webdriver/remote/isDisplayed.js
              '';
            });
          }; in with pkgs; python3.override { inherit packageOverrides; };
    in
    {
      packages.x86_64-linux.default = pkgs.mkShell {
        packages =
          with pkgs;
          [
            (python'.withPackages (ps: with ps; [
              requests
              ipython
              ipdb
              beautifulsoup4
              selenium
            ]))
            nixpkgs-fmt
          ];
      };
    };
}
