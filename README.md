Make sure you've got Firefox installed.

1. Install the [nix package manager](https://nixos.org/download.html)
2. Enable "flakes" feature in nix: https://nixos.wiki/wiki/Flakes
3. Go to the directory of this project and run `nix develop`. This may take while the first time.
4. You are now in a shell that has all packages available to run the script.
5. From within the shell run `python scrape.py`

What is left to do?
- Add more search engines
- Get multiple pages from results. This is tricky. E.g. for duckduckgo rou need to click the "next page" button. 
  See here how to click https://www.selenium.dev/documentation/webdriver/actions_api/mouse/#click-and-release
  For Bing to you need to scroll to the bottom a few times https://www.selenium.dev/documentation/webdriver/actions_api/wheel/
